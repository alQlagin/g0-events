const app = require('./server/server');
const tg = require('./telegram');
const cluster = require('cluster');
const BaseScopeExtension = require('telegram-node-bot').BaseScopeExtension;

class AppConnectorExtension extends BaseScopeExtension {
    process() {
        return app
    }

    get name() {
        return 'getApp'
    }
}

tg.addScopeExtension(AppConnectorExtension);

function start() {
    if (!app.booting) {
        if (cluster.isMaster) {
            app.start();
        }
    }
    return !app.booting;
}

const interval = setInterval(() => {
    if (start()) {
        clearInterval(interval);
    } else {
        console.log('.')
    }
}, 100);
