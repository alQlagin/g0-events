'use strict';

const Telegram = require('telegram-node-bot');
const TextCommand = Telegram.TextCommand;
const tg = new Telegram.Telegram('113958393:AAF6xF8PZFDwvO7CwZXghyeXTrqT8ZSW4Kc'); // @alexkBot

const StartController = require('./controllers/start');
const EventsController = require('./controllers/events');
const DefaultController = require('./controllers/default');

module.exports = tg;
const eventsCtrl = new EventsController();
tg.router
    .when(new TextCommand('/start'), new StartController())
    .when(new TextCommand('/today_events', 'todayEventsHandler'), eventsCtrl)
    .when(new TextCommand('/opened_events', 'openedEventsHandler'), eventsCtrl)
    .otherwise(new DefaultController())

