const {TelegramBaseController} = require('telegram-node-bot');

class StartController extends TelegramBaseController {
    handle($) {
        /**
         * @TODO добавить регистрацию через $.getApp().models.BotUser.register
         */
        $.getApp().models.BotUser.register();

        $.sendMessage('Привет');

    }
}

module.exports = StartController;