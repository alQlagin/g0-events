const {TelegramBaseController} = require('telegram-node-bot');

class EventsController extends TelegramBaseController {
    todayEvents($) {
        /**
         * @TODO поиск сегодняшних событий через $.getApp().models.G0_Events.findToday
         */
        $.sendMessage('todayEvents!');
        /**
         * @TODO вывести в цикле через
         *      $.sendMessage('%Название%\nВедущий: %имя ведещуего%\nДата проведения: %дата%\nМесто проведения: %место%')
         *      поля с информацией лежат в event._info
         */
    }

    openedEvents($) {
        /**
         * @TODO поиск событий c открытой регистрацией через $.getApp().models.G0_Events.findOpened
         */
        $.sendMessage('openedEvents!');
        /**
         * @TODO вывести в цикле через
         *      $.sendMessage('%Название%\nВедущий: %имя ведещуего%\nДата проведения: %дата%\nМесто проведения: %место%')
         *      поля с информацией лежат в event._info
         */
    }

    get routes() {
        return {
            'todayEventsHandler': 'todayEvents',
            'openedEventsHandler': 'openedEvents'
        }
    }
}

module.exports = EventsController;