'use strict';

module.exports = function (G0event) {
    G0event.findToday = findToday;
    G0event.findOpened = findOpened;

    /**
     * @returns {Promise.<T>}
     */
    function findOpened() {
        /**
         * @TODO найти события на которые открыта регистрация (текущая дата между startRegistrationDate и endRegistrationDate)
         */

        return Promise.resolve();
    }

    /**
     * @returns {Promise.<T>}
     */
    function findToday() {
        /**
         * @TODO найти события которые будут проходить сегодня (поле eventDate)
         */

        return Promise.resolve();
    }
};
